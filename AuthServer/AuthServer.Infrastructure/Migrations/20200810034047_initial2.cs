﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuthServer.Infrastructure.Migrations
{
    public partial class initial2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "27e8d4aa-67ca-48af-a986-7059029dbefb");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "9b968f9b-b421-4ed7-a960-08285d365136", "ed63a201-1b60-4c10-b2a9-171a5beb01b2", "consumer", "CONSUMER" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9b968f9b-b421-4ed7-a960-08285d365136");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "27e8d4aa-67ca-48af-a986-7059029dbefb", "96def603-94ae-4f01-b766-fc2bee1f06ab", "consumer", "CONSUMER" });
        }
    }
}
