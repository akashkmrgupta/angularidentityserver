﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuthServer.Infrastructure.Migrations
{
    public partial class initial3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9b968f9b-b421-4ed7-a960-08285d365136");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "41908076-690d-4fef-9ac7-17f737f6f86e", "b861e0a3-d67d-4b14-8d8a-96e29b00b2f6", "consumer", "CONSUMER" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "41908076-690d-4fef-9ac7-17f737f6f86e");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "9b968f9b-b421-4ed7-a960-08285d365136", "ed63a201-1b60-4c10-b2a9-171a5beb01b2", "consumer", "CONSUMER" });
        }
    }
}
