## Development Environment
- Visual Studio 2019 Community
- Visual Studio Code 1.32.3 
- .NET Core SDK 3.1 
- Angular 8
- IdentityServer4 3.0.1
- SQL Server Express 2016

## Setup

#### To run the demo:

**1.** Run DB Script in root folder.

**2.** Change Connection String of the Solution AuthServer.sln & Resource.Api.sln.

**3.** Install Angular CLI if necessary. `npm install -g @angular/cli`

**4.** Spa\oauth-client> npm install

**5.** Spa\oauth-client> ng serve

**6.** AngularIdentityServer\AuthServer> dotnet run --url="http://localhost:5000" --projecct="AuthServer"

**7.** AngularIdentityServer\Resource.Api> dotnet run --url="http://localhost:5050" --project="Resource.Api" 

**8.** Locate a browser to `http://localhost:4200` to access the Angular client.
 

